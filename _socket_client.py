import socket

UDP_IP = "212.224.112.240"
UDP_PORT = 7701
MESSAGE = "127.0.0.1:5600:data takoe data"

#print "UDP target IP:", UDP_IP
#print "UDP target port:", UDP_PORT
#print "message:", MESSAGE

sock = socket.socket( socket.AF_INET, # Internet
                      socket.SOCK_DGRAM ) # UDP
sock.sendto( MESSAGE, (UDP_IP, UDP_PORT) )
print sock.recv(1024)