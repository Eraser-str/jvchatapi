import datetime
import socket

UDP_IP = "212.224.112.240"
UDP_PORT = 7701

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))


def log_write(data):
    with open('socket_log.txt', 'a') as f:
        f.write(str(datetime.datetime.now()) + "\t" + data + "\n")


while True:
    data, addr = sock.recvfrom(2048)
    data = str(data).split(":")
    print "data:", data, " / ", addr
    log_write("data: " + str(data) + " / " + addr)  # DEBUG LOG
    resp = buffer("00100000" + str(addr[0]) + ", " + str(addr[1]))
    try:
        sock.sendto(buffer(str(data[2:])), tuple([data[0], int(data[1])]))
    except:
        resp = buffer("10100000" + str(addr[0]) + ", " + str(addr[1]))
    print resp
    log_write("resp: " + str(resp))  # DEBUG LOG
    sock.sendto(resp, addr)