# -*- coding: utf-8 -*-
# TODO: добавить ошибку, если аргументы не переданы (request.query_string)
# TODO: написать скрипт, который будет проходить по коллекции wait и переносить оттуда в коллекцию queue всех, кто там уже больше минуты

from flask import request, jsonify, redirect, render_template

from config import *
from models import Responses, get_current_time, db, str_gen


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    return render_template('index.html')

@app.route('/<page>')
def page(page):
    return render_template(page)


@app.route(API_FULL_PREFIX_V1 + 'chat/entry')
def in_chat():
    """Метод используется для соединения двух пользователей между собой. При обращении, метод находит случайного
    пользователя для общения среди удовлетворяющих требованию запроса (gender_like со стороны обратившегося и
    ожидающего) и возвращает его device_id, либо добавляет пользователя в список ожидающих если пользователей,
    удовлетворяющих запросу, не найдено.

    Пример хорошего запроса:
    ------------------------
    *http://127.0.0.1:5000/api/v1/chat/entry?device_id=d3v1c3_1d_5tr1g&gender=female&gender_like=male*

    Примера запроса, вызывающего ошибку 1001:
    -----------------------------------------
    *http://127.0.0.1:5000/api/v1/chat/entry?device_id=d3v1c3_1d_5tr1g&gender=female&gender_like=1*


    :param device_id: идентификатор устройства, необходимый для соединения двух устройств и передачи контента между ними
    :param gender: пол пользователя (male - мужской, female - женский)
    :param gender_like: сексуальные предпочтения пользователя (подбирать мужчин/женщин)

    :type device_id: str
    :type gender: str
    :type gender_like: str

    :return: device_id пользователя, с которым нужно соединяться или просто ответ об успешном выполнении операции
    (и создании записи по средствам 201-го кода состояния http)
    :rtype: json

    :raise 1001: gender and gender_like arguments must be 'male' or 'female [http code: 400]

    """
    device_id = request.args.get('device_id')
    gender = request.args.get('gender')
    gender_like = request.args.get('gender_like')

    if gender not in ['male', 'female'] \
    or gender_like not in ['male', 'female']:
        resp = Responses.bad_request(1001,
                                     "gender and gender_like arguments must be 'male' or 'female' (received %s and %s)"
                                     % (gender, gender_like)
        )
        return jsonify(resp), 400

    user = db.queue.find_one({'gender': gender_like, "gender_like": gender})
    if user is not None:
        db.queue.remove({"_id": user['_id']})
        user['select_time'] = get_current_time()
        db.wait.insert(user)
        return jsonify(user), 200
    else:
        # Генерируем уникальный идентификатор
        u_id = str_gen(32)
        while db.queue.find_one({"_id": u_id}) is not None:
            u_id = str_gen(32)
        query_user = {
            "_id": u_id,
            "device_id": device_id,
            "gender": gender,
            "gender_like": gender_like,
            "add_time": get_current_time()
        }
        # Пишем пользователя в очередь
        db.queue.insert(query_user)
        return jsonify(Responses.success(201)), 201

    # Если мы дошли до этого места, значит где-то выше я что-то не просчитал.
    return {"error": "err"}


@app.route(API_FULL_PREFIX_V1 + 'connection/confirm')
def confirm_connection():
    """
    Метод, подтверждающий успешное соединение двух пользователей. Вызывается после использования метода chat/entry и
    успешной установки p2p соединения между пользователями

    **Пример хорошего запроса:**

    `http://127.0.0.1:5000/api/v1/connection/confirm?device_id=d3v1c3_1d_5tr1g`_


    :param devive_id: device_id пользователя, полученного по средствам вызова метода /chat/entry

    :type device_id: str

    :return: 2001 error или сообщение об успещном запросе
    :rtype: json

    :raise 2001: device_key is not found. may limit expectations of one minute expired [http code: 400]

    """
    user = db.wait.find_one({"device_id": request.args.get('device_id')})
    if user is None:
        resp = Responses.bad_request(2001,
                                     "device_key is not found. may limit expectations of one minute expired")
        return jsonify(resp), 400
    # Удалим пользователя из списка ожидающих
    db.wait.remove({"_id": user['_id']})
    # Добавим пользователю ключ начала разговора с текущем временем
    user['start_talk'] = get_current_time()
    # Добавим пользователя в журнал бесед
    db.call_log.insert(user)

    return jsonify(Responses.success(200)), 200


@app.route(API_FULL_PREFIX_V1 + 'connection/refuse')
def refuse_connection():
    """
    Метод служит для разрыва соединения. Вызывать его стоит в случае желания сменить собеседника (кнопка 'next'), либо
    при неудавшейся попытке наладить связь между двумя устройствами.

    :param device_id: device_id пользователя, полученного по средствам вызова метода /chat/entry и соеденного по
    средствам вызова метода connection/confirm.

    :type device_id: str

    :return: 2001 error или сообщение об успещном запросе
    :rtype: json

    :raise 2001: device_key is not found. conversation with the device %s is not registered [http code: 400]

    """
    user = db.call_log.find_one({"device_id": request.args.get('device_id')})
    if user is None:
        resp = Responses.bad_request(2001,
                                     "device_key is not found. conversation with the device %s is not registered"
                                     % request.args.get('device_id')
        )
        return jsonify(resp), 400
    print user
    db.call_log.update({"_id": user['_id']}, {'$set': {"end_talk": get_current_time()}})
    print db.call_log.find_one({"_id": user['_id']})

    return jsonify(Responses.success(200)), 200


# METHODS FOR DEBUG

@app.route(API_FULL_PREFIX_V1 + 'users/clear')
def clear_all_users():
    """
    Отладочный метод, очищающий все коллекции
    :return:
    """
    db.queue.remove({})
    db.wait.remove({})
    db.call_log.remove({})
    return jsonify(Responses.success(200)), 200


@app.route(API_FULL_PREFIX_V1 + 'socket/log')
def socket_log():
    with open('/socket_log.txt') as f:
        for i in f.readlines():
            print i
    return "qq"