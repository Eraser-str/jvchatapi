import time

from models import db, get_current_time


while True:
    db.wait.remove({"select_time": {'$lte': get_current_time() - 60}})
    time.sleep(15)