from flask import Flask

# variables
app = Flask(__name__)

API_PREFIX = "api"
API_V1 = 'v1'
API_FULL_PREFIX_V1 = '/' + API_PREFIX + '/' + API_V1 + '/'

API_V_CURRENT = API_V1