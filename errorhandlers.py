from flask import jsonify, request

from config import app
from models import Responses

@app.errorhandler(404)
def not_found(error):
    return jsonify(Responses.not_found(request.path)), 404