import time
import random
from string import hexdigits, letters

import pymongo


class Responses:
    def bad_request(self, err_code, err_val):
        return {
            "response": {
                "http_status_code": 400,
                "error": [err_code, err_val],
                "done": False
            }
        }

    def not_found(self, method):
        return {
            "response": {
                "http_status_code": 404,
                "error": [404, "Method %s not found. Refer to the documentation" % method],
                "done": False
            }
        }

    def success(self, http_code):
        return {
            "response": {
                "http_status_code": http_code,
                "done": True
            }
        }
Responses = Responses()


def get_current_time():
    return time.time()


def str_gen(length):
    return "".join(random.choice(letters + hexdigits) for _ in range(length))


def db_connect():
    conn = pymongo.Connection('localhost', 27017)
    db = conn['takaya_db']
    return db
db = db_connect()