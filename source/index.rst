.. JVchat documentation master file, created by
   sphinx-quickstart on Sun Feb  9 16:55:26 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to JVchat documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   api_methods.rst
   api_errors.rst

An example of accessing api
---------------------------
http://212.224.112.240:7700/api/v1/<method>[?arg1=val1&arg2=val2]

Current api version
-------------------
1

Api prefix
----------
/api/v1/

Api port
--------
7700

Api domain
----------
212.224.112.240
