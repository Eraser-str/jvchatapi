API Methods
===========

chat/entry
----------

.. py:function:: chat/entry

   Метод используется для соединения двух пользователей между собой. При обращении, метод находит случайного
   пользователя для общения среди удовлетворяющих требованию запроса (gender_like со стороны обратившегося и
   ожидающего) и возвращает его device_id, либо добавляет пользователя в список ожидающих если пользователей,
   удовлетворяющих запросу, не найдено.

   **Пример хорошего запроса:**

   `<http://127.0.0.1:5000/api/v1/chat/entry?device_id=d3v1c3_1d_5tr1g&gender=female&gender_like=male>`_

   **Примера запроса, вызывающего ошибку 1001:**

   `<http://127.0.0.1:5000/api/v1/chat/entry?device_id=d3v1c3_1d_5tr1g&gender=female&gender_like=1>`_


   :param device_id: идентификатор устройства, необходимый для соединения двух устройств и передачи контента между ними
   :param gender: пол пользователя (male - мужской, female - женский)
   :param gender_like: сексуальные предпочтения пользователя (подбирать мужчин/женщин)

   :type device_id: str
   :type gender: str
   :type gender_like: str

   :return: device_id пользователя, с которым нужно соединяться или просто ответ об успешном выполнении операции
       (и создании записи по средствам 201-го кода состояния http)
   :rtype: json

   :raise 1001: gender and gender_like arguments must be 'male' or 'female [http code: 400]


connection/confirm
------------------

.. py:function:: connection/confirm

   Метод, подтверждающий успешное соединение двух пользователей. Вызывается после использования метода chat/entry и
   успешной установки p2p соединения между пользователями

   **Пример хорошего запроса:**

   `<http://127.0.0.1:5000/api/v1/connection/confirm?device_id=d3v1c3_1d_5tr1g>`_


   :param devive_id: device_id пользователя, полученного по средствам вызова метода chat/entry

   :type device_id: str

   :return: 2001 error или сообщение об успещном запросе
   :rtype: json

   :raise 2001: device_key is not found. may limit expectations of one minute expired [http code: 400]


connection/refuse
-----------------

.. py:function:: connection/refuse

    Метод служит для разрыва соединения. Вызывать его стоит в случае желания сменить собеседника (кнопка 'next'), либо
    при неудавшейся попытке наладить связь между двумя устройствами.

    **Пример хорошего запроса:**

    `<http://127.0.0.1:5000/api/v1/connection/refuse?device_id=d3v1c3_1d_5tr1g>`_


    :param device_id: device_id пользователя, полученного по средствам вызова метода chat/entry и соединенного по
                        средствам вызова метода connection/confirm.

    :type device_id: str

    :return: 2001 error или сообщение об успещном запросе
    :rtype: json

    :raise 2001: device_key is not found. conversation with the device %s is not registered [http code: 400]